# Franz Recipe Bitbucket

Use your Bitbucket Account in Franz.

### About

* Only for Atlassian Bitbucket Account
* Version 1.0.0

### How do I get set up? (Linux)
* cd ~/.config/Franz/recipes/dev
* (Note that this dev directory may not exist yet, and you must create it `mkdir -p ~/.config/Franz/recipes/dev`)
* Clone this repository `git clone https://gitlab.com/bloomcake/franz-bitbucket.git`
* Restart Franz and add new Service
